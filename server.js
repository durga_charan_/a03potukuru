var path = require("path");
var express = require("express");
var logger = require("morgan");
var bodyParser = require("body-parser");

var app = express();  // make express app
var server = require('http').Server(app); // inject app into the server

// 1 set up the view engine
app.set("views", __dirname+'/assets') // path to views
app.set("view engine", "ejs") // specify our view engine
app.use(express.static(__dirname+'/assets'))
// 2 create an array to manage our entries
var entries = []
app.locals.entries = entries // now entries can be accessed in .ejs files

// 3 set up an http request logger to log every request automagically
app.use(logger("dev"))     // app.use() establishes middleware functions
app.use(bodyParser.urlencoded({ extended: false }))

// 4 handle http GET requests (default & /new-entry)
app.get("/index", function (request, response) {
  response.render(__dirname+"/assets/index")
})
app.get("/new-entry", function (request, response) {
  response.render(__dirname+"/assets/new-entry")
})
app.get("/Potukuru_DurgaCharan", function (request, response) {
  response.render(__dirname+"/assets/Potukuru_DurgaCharan")
})
app.get("/contact", function (request, response) {
  response.render(__dirname+"/assets/Contact")
})

app.get("/MyChoice", function (request, response) {
  response.render(__dirname+"/assets/MyChoice")
})
app.get("/", function (request, response) {
  response.render(__dirname+"/assets/Potukuru_DurgaCharan")
})
// 5 handle an http POST request to the new-entry URI 
/*app.post("/new-entry", function (request, response) {
  var api_key = 'key-d55a0f5aae160d5ab5615952d04949f1';
  var domain = 'sandboxc0cf19b4a061464babaeb352dc26a40d.mailgun.org';
  var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
   
  var data = {
    from: 'charan <postmaster@sandboxc0cf19b4a061464babaeb352dc26a40d.mailgun.org>',
    to: 'S530748@nwmissouri.edu',
    subject: request.body.title,
    text: request.body.body,
  };
   
  mailgun.messages().send(data, function (error, body) {
    console.log(body);

    if(!error)
      response.send("Done!")
    else
      response.send("fucked up somewhere!")

  });
});*/

//mailing contact page info ra baabu
app.post("/Contact", function (request, response) {
  var api_key = 'key-d55a0f5aae160d5ab5615952d04949f1';
  var domain = 'sandboxc0cf19b4a061464babaeb352dc26a40d.mailgun.org';
  var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
   
  var data = {
    from: 'charan <postmaster@sandboxc0cf19b4a061464babaeb352dc26a40d.mailgun.org>',
    to: 'S530748@nwmissouri.edu',
    subject: request.body.name,
     //text: request.body.query + request.body.boxx, 
html: "<b>Questions: </b>" + request.body.query +"<b>    From: <b>" + request.body.email
  };
   
  mailgun.messages().send(data, function (error, body) {
    console.log(body);

    if(!error)
      response.send("Done!")
    else
      response.send("Messed up somewhere!")

  });
});

app.post("/new-entry", function (request,response){
    if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.")
    return
  }
  entries.push({  // store it
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  })
  response.redirect("/index")  // where to go next? Let's go to the home page :)
})

// if we get a 404 status, render our 404.ejs view
app.use(function (request, response) {
  response.status(404).render("404")
})

// Listen for an application request on port 8081
server.listen(8081, function () {
  console.log('Guestbook app listening on http://127.0.0.1:8081/');
});